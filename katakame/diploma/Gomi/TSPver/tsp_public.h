#ifndef TSP_PUBLIC_H_INCLUDED
#define TSP_PUBLIC_H_INCLUDED

#include "tsp.h"

/***************/
/* static 変数 */
/***************/
/* スケーリングウィンドウ */
#define WINDOW_SIZE 5 /* スケーリングウィンドウサイズ */
static double scaling_window[WINDOW_SIZE];

/****************************/
/* 各関数のプロトタイプ宣言 */
/* 実体は tsp_private.c 内  */
/* tsp_public.c 内で使用    */
/****************************/

/* 遺伝子型を表現型に変換 */
void gtype_to_ptype(int g[CITY_NUM], int p[CITY_NUM]);

/* 遺伝子を表示 */
void print_gene(int gene[GENE_NUM]);

/* 経路を表示 */
void print_route(int route[CITY_NUM]);

/* 各個体の経路長を計算 */
void calculate_length(Gene_set *gene_ptr, Position city[CITY_NUM]);

/* 最良の遺伝子番号を得る */
int get_best_gene(Gene_set gene_group[GENE_NUM]);
#endif /* TSP_PUBLIC_H_INCLUDED */
