#ifndef TSP_H_INCLUDED
#define TSP_H_INCLUDED

#define YES 1
#define NO 0
#define CITY_NUM 10 /* 都市数 */
#define POSITION_MAX 10 /* 都市座標の最大値 */

/* 以下のパラメータを調節する */
#define GENE_NUM 100 /* 個体数 */
#define ELETE 0 /* エリート戦略用 */
#define GENERATION 200 /* 進化世代数 */
#define MUTATION_RATIO 0.1 /* 突然変異確率 */
#define CROSSOVER_RATIO 0.3 /* 交叉確率 */ /* 本実験では不使用 */ 

/****************/
/* 構造体の定義 */
/****************/
/* 都市座標 */
typedef struct position_tag {
	int x; /* x座標 */
	int y; /* y座標 */
} Position;

/* 個体 */
typedef struct gene_set_tag {
	int gene[CITY_NUM]; /* 遺伝子 */
	double length; /* 経路長 */
	double fitness; /* 適合度 */
} Gene_set;

/********************************/
/* 実験作成関数プロトタイプ宣言 */
/********************************/
/* 突然変異 */
void mutation(Gene_set gene_group[GENE_NUM]);

/* 選択 */
void selection(Gene_set gene_group[GENE_NUM]);

/****************************/
/* 各関数のプロトタイプ宣言 */
/* 実体は tsp_public.c 内   */
/* main 関数内で使用        */
/****************************/

/* 都市をランダムに設定 */
void initial_city(Position city[CITY_NUM]);

/* 遺伝子をランダムに設定 */
void initial_gene(Gene_set gene_group[GENE_NUM]);

/* 適応度の初期設定 */
void initial_fitness(Gene_set *gene_ptr, Position city[CITY_NUM]);

/* 適応度の計算 */
void calculate_fitness(Gene_set *gene_ptr, Position city[CITY_NUM]);

/* 都市の座標を表示 */
void print_city(Position city[CITY_NUM]);

/* 各個体のデータを表示 */
void print_gene_group(Gene_set gene_group[GENE_NUM]);

/* 最良解を表示 */
void print_best_data(Gene_set gene_group[GENE_NUM]);

/* 最良値、平均値、最悪値を表示 */
void print_data(Gene_set gene_group[GENE_NUM]);
#endif /* define TSP_H_INCLUDED */
