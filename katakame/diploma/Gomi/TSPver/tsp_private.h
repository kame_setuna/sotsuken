#ifndef TSP_PRIVATE_H_INCLUDED
#define TSP_PRIVATE_H_INCLUDED
#include "tsp.h"

/******************************/
/* 各関数のプロトタイプ宣言   */
/* 実体は tsp_private.c 内    */
/* tsp_private.c 内でのみ使用 */
/******************************/

/* 距離を計算 */
double distance(Position city1, Position city2);

/* 経路長を計算 */
double path_length(Position city[CITY_NUM], int gene[CITY_NUM]);

/* 遺伝子を複写 */
void copy_gene(int gene_to[CITY_NUM], int gene_from[CITY_NUM]);

/* 個体の交換 */
void swap_gene_set(Gene_set gene_group[GENE_NUM], int i, int j);

/* 最適解の遺伝子番号を得る */
int get_best_gene(Gene_set gene_group[GENE_NUM]);

#endif /* define TSP_PRIVATE_H_INCLUDED */
