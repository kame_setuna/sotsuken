#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "tsp.h"

#define PRINT_GENE NO /* 遺伝子を表示（本来はオプションで指示するもの） */
#define PRINT_PROCESS YES /* 途中経過を表示（本来はオプションで指示するもの） */

void print_usage(char *command) /* 使用方法を表示する関数 */
{
	fprintf(stderr, "Usage: %s <filename>\n", command);
}

int main(int argc, char *argv[])
{
	int i; /* カウンタ用 */
	Position city[CITY_NUM]; /* 都市座標 */
	Gene_set gene_group[GENE_NUM]; /* 遺伝子集団 */
	FILE *fp; /* 座標用ファイル */
	char string[BUFSIZ]; /* ファイル入力用バッファ */

	srand((unsigned int)time(NULL)); /* rand() の種 */

	/* コマンド引数処理 */
	if (argc > 2) { /* コマンド引数が多すぎる */
		print_usage(argv[0]); /* 使用方法を表示 */
		exit(1); /* 終了 */
	}
	/* コマンド引数が無い時 */
	else if (argc == 1) {
		initial_city(city); /* ランダムに都市を生成 */
		fprintf(stderr, "RANDOM COORDINATES ARE USED!!\n");
	}
	/* コマンド引数が 1つ */
	/* ファイルから都市座標を読みこみ */
	else if(argc == 2) {
		if((fp = fopen(argv[1], "r")) == NULL) {
			/* ファイルオープン失敗 */
			fprintf(stderr, "%s: %s can not be found\n", argv[0], argv[1]);
			exit(1);
		}
		for(i = 0; (i < CITY_NUM) && (fgets(string, BUFSIZ, fp) != NULL);) { /* ファイルの終わりか都市数が設定に達するまで */
			if((string[0] == '#')||(string[0] == '\n')) {
				/* コメント行および空行 */
				continue; /* 何も処理しない */
			}
			/* データ読み込み */
			sscanf(string, "%d %d", &(city[i].x), &(city[i].y));
			i++; /* 読みこんだ都市数を増やす */
		}
		fclose(fp);
	}

	/* 各種データの初期化 */
	initial_gene(gene_group); /* 遺伝子集団の生成 */
	initial_fitness(gene_group, city); /* 初期適合度の計算 */

	/* 初期データの表示 */
	print_city(city);
#if PRINT_GENE /* 1/2 */
	print_gene_group(gene_group);
#endif /* PRINT_GENE 1/2 */

	printf("INITIAL RESULT\n");
	print_best_data(gene_group);

	for(i = 0; i < GENERATION; i++) {
		/* 選択 */
		selection(gene_group); /* この関数を実験で作成 */

		/* 突然変異を起こす */
		mutation(gene_group); /* この関数を実験で作成 */

		/* 適合度計算 */
		calculate_fitness(gene_group, city);

#if PRINT_PROCESS
		/* 進化中のデータ表示 */
		printf("GENERATION %d\n", i);
		print_data(gene_group);
		/* print_best_data(gene_group); */
#endif /* PRINT_PROCESS */
	}

	/* 進化終了時のデータ表示 */
	printf("FINAL RESULT\n");
#if PRINT_GENE /* 2/2 */
	print_gene_group(gene_group);
#endif /* PRINT_GENE 2/2 */
	print_best_data(gene_group);

	return 0;
}
