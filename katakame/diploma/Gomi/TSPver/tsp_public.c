#include <stdio.h>
#include <stdlib.h>
#include "tsp_public.h"

/* 都市座標の初期化 */
/* 配列 city[] にランダムに都市座標を設定 */
void initial_city(Position city[CITY_NUM]) {
	int i;
	for(i = 0; i < CITY_NUM; i++) {
		city[i].x = rand()%POSITION_MAX;
		city[i].y = rand()%POSITION_MAX;
	}
};

/* 遺伝子の初期化（ランダム） */
/* 構造体 Gene_set 中の gene[] に遺伝子をランダムに設定 */
void initial_gene(Gene_set gene_group[GENE_NUM]) {
	int i, j;
	for(i = 0; i < GENE_NUM; i++) {
		for(j = 0; j < CITY_NUM; j++) {
			/* 巡回表現を用いる */
			gene_group[i].gene[j] = rand()%(CITY_NUM - j);
		}
	}
}

/* 適合度の初期化 */
void initial_fitness(Gene_set *gene_ptr, Position city[CITY_NUM])
{
	int i; /* カウンタ */
	/* スケーリングウィンドウ関連 */
	/* （スケーリングウィンドウ自体はstatic変数）*/
	double length_max; /* 経路長の最大値（スケーリングウィンドウ用）*/

	/* 全遺伝子の経路長計算 */
	calculate_length(gene_ptr, city);

	for(i = 0; i < GENE_NUM; i++) { /* 経路長の最大値の計算 */
		if(i == 0) {
			length_max = gene_ptr->length;
		}
		else if((gene_ptr + i)->length > length_max) {
			length_max = (gene_ptr + i)->length;
		}
	}

	for(i = 0; i < WINDOW_SIZE; i++) { /* スケーリングウィンドウの設定 */
		/* 最初は全部同じ値にする */
		scaling_window[i] = length_max;
	}

	for(i = 0; i < GENE_NUM; i++) {
		/* 適合度は 経路長/スケーリングウィンドウ内の最大値 */
		(gene_ptr + i)->fitness = (gene_ptr + i)->length / length_max;
	}

}

/* 適合度の計算 */
void calculate_fitness(Gene_set *gene_ptr, Position city[CITY_NUM])
{
	int i;
	double length_max;

	/* 遺伝子集団の経路長を求める */
	calculate_length(gene_ptr, city);

	/* 現在の遺伝子集団の最大経路長を求める */
	for(i = 0; i < GENE_NUM; i++) {
		if(i == 0) {
			length_max = gene_ptr->length;
		}
		else if((gene_ptr + i)->length > length_max) {
			length_max = (gene_ptr + i)->length;
		}
	}

	/* スケーリングウィンドウの更新 */
	for(i = 0; i < WINDOW_SIZE; i++) {
		if (i < WINDOW_SIZE-1) {
			scaling_window[i] = scaling_window[i+1];
		}
		else {
			scaling_window[i] = length_max;
		}
	}

	/* スケーリングウィンドウ中の最大経路長を求める */
	for(i = 0; i < WINDOW_SIZE; i++) {
		if (scaling_window[i] > length_max) {
			length_max = scaling_window[i];
		}
	}

	/* 遺伝子集団の適合度を求める */
	for(i = 0; i < GENE_NUM; i++) {
		(gene_ptr + i)->fitness = (gene_ptr + i)->length / length_max;
	}

}

/* 都市座標の表示 */
void print_city(Position city[CITY_NUM]) {
	int i;
	for(i = 0; i < CITY_NUM; i++) {
		printf("city%03d %d %d\n", i, city[i].x, city[i].y);
	}
}


/* 遺伝子のデータ表示 */
void print_gene_group(Gene_set gene_group[GENE_NUM]) {
	int i;
	int route[CITY_NUM];
	
	for(i = 0; i < GENE_NUM; i++) {
		gtype_to_ptype(gene_group[i].gene, route);
		print_gene(gene_group[i].gene); /* 遺伝子型 */
		print_route(route); /* 表現型 */
		printf("length = %f (fitness = %f)\n", gene_group[i].length, gene_group[i].fitness); /* 経路長と適合度 */
	}
}

/* 現時点の最良解の表示 */
void print_best_data(Gene_set gene_group[GENE_NUM])
{
	int route[CITY_NUM];
	int best_index; /* 最良解の遺伝子番号 */

	best_index = get_best_gene(gene_group);

	print_gene(gene_group[best_index].gene); /* 遺伝子表示 */
	gtype_to_ptype(gene_group[best_index].gene, route);
	print_route(route); /* 巡回路表示 */
	printf("length = %f (fitness = %f)\n", gene_group[best_index].length, gene_group[best_index].fitness); /* 経路長・適合度表示 */
}

/* データの表示 */
void print_data(Gene_set gene_group[GENE_NUM])
{
	double best, sum, worst;
	int i;
	
	sum = 0;
	best = worst = gene_group[0].length;
	for(i = 0; i < GENE_NUM; i++) {
		sum += gene_group[i].length; /* 平均を出すため合計を求める */
		if (best > gene_group[i].length) {
			best = gene_group[i].length; /* 最良値 */
		}
		if (worst < gene_group[i].length) {
			worst = gene_group[i].length; /* 最悪値 */
		}
	}
	printf("best = %g, ave = %g, worst = %g\n", best, sum/GENE_NUM, worst);
}
