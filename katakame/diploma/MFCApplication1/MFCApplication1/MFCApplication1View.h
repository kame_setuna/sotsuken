
// MFCApplication1View.h : CMFCApplication1View クラスのインターフェイス
//

#pragma once


class CMFCApplication1View : public CHtmlView
{
protected: // シリアル化からのみ作成します。
	CMFCApplication1View();
	DECLARE_DYNCREATE(CMFCApplication1View)

// 属性
public:
	CMFCApplication1Doc* GetDocument() const;

// 操作
public:

// オーバーライド
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 構築後に初めて呼び出されます。

// 実装
public:
	virtual ~CMFCApplication1View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成された、メッセージ割り当て関数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCalstart();
	static UINT ThreadControll(LPVOID pParam);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CWinThread* m_pThread;
};

#ifndef _DEBUG  // MFCApplication1View.cpp のデバッグ バージョン
inline CMFCApplication1Doc* CMFCApplication1View::GetDocument() const
   { return reinterpret_cast<CMFCApplication1Doc*>(m_pDocument); }
#endif

