
// MFCApplication1View.cpp : CMFCApplication1View クラスの実装
//

#include "stdafx.h"

#define _CRT_SECURE_NO_WARNINGS
// SHARED_HANDLERS は、プレビュー、縮小版、および検索フィルター ハンドラーを実装している ATL プロジェクトで定義でき、
// そのプロジェクトとのドキュメント コードの共有を可能にします。
#ifndef SHARED_HANDLERS
#include "MFCApplication1.h"
#endif



#include "MFCApplication1Doc.h"
#include "MFCApplication1View.h"

#include "stdHeader.h"
#include "Header.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCApplication1View

IMPLEMENT_DYNCREATE(CMFCApplication1View, CHtmlView)

BEGIN_MESSAGE_MAP(CMFCApplication1View, CHtmlView)
	ON_COMMAND(ID_CalStart, &CMFCApplication1View::OnCalstart)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CMFCApplication1View コンストラクション/デストラクション

CMFCApplication1View::CMFCApplication1View()
{
	// TODO: 構築コードをここに追加します。

}

CMFCApplication1View::~CMFCApplication1View()
{
}

BOOL CMFCApplication1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

	return CHtmlView::PreCreateWindow(cs);
}

void CMFCApplication1View::OnInitialUpdate()
{
	CHtmlView::OnInitialUpdate();

	Navigate2(_T("http://kamesetuna.s602.xrea.com/"));
}


// CMFCApplication1View 診断

#ifdef _DEBUG
void CMFCApplication1View::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CMFCApplication1View::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CMFCApplication1Doc* CMFCApplication1View::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCApplication1Doc)));
	return (CMFCApplication1Doc*)m_pDocument;
}
#endif //_DEBUG


// CMFCApplication1View メッセージ ハンドラー

C course[1000];
double mdistance;
int discrete, num;

void myExit() {
	MessageBox(NULL,_T("Couldn't open the file."), _T("Error!!"), MB_ICONERROR | MB_OK);
	exit(EXIT_FAILURE);
}



void CMFCApplication1View::OnCalstart() {
	// TODO: ここにコマンド ハンドラー コードを追加します。


	AfxBeginThread(ThreadControll, this);

}


UINT CMFCApplication1View::ThreadControll(LPVOID pParam) {

	int i = 0, j;
	FILE *fp_course;
	Gene gene_group[GENE_NUM];

	if(fopen_s(&fp_course, "courseprofile.txt", "r")) {
		fprintf_s(stderr, "Error!");
		exit(EXIT_FAILURE);
	}


	course[0].dis = course[0].grad = 0;
	i = 1;

	while(2 == fscanf_s(fp_course, "%lf %lf", &course[i].dis, &course[i].grad)) {
		course[i].dis = 200;
		i++;
	}

	fclose(fp_course);

	for(j = 2; j < i; j++)
		course[j].dis += course[j - 1].dis;


	mdistance = course[i - 1].dis;
	discrete = 200;

	num = i;

	srand((unsigned int)time(NULL));

	initialize(gene_group);
	calculate_fitness(gene_group);

	for(i = 0; i < GENERATION; i++) {
		selection(gene_group);
		makeChildren(gene_group);
		mutation(gene_group);
		calculate_fitness(gene_group);
	}

	i = find_best(gene_group, 0);
	writeData(&gene_group[i]);
	AfxMessageBox(_T("計算終了"));


	return 0;
}


void CMFCApplication1View::OnTimer(UINT_PTR nIDEvent) {
	// TODO: ここにメッセージ ハンドラー コードを追加するか、既定の処理を呼び出します。



	DWORD result = WaitForSingleObject(m_pThread->m_hThread, 0);

	if(result == WAIT_ABANDONED || result == WAIT_OBJECT_0) {
		CWnd::KillTimer(1);
		MessageBox(_T("終了しました"), _T("計算終了"), MB_OK);
		delete m_pThread;
	}
	CHtmlView::OnTimer(nIDEvent);
}
