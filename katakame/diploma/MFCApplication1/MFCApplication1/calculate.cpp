#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS
#include "stdHeader.h"
#include "Header.h"

#define RESFALSE NO

using namespace std;

extern C course[1000];
extern double mdistance;
extern int discrete;
extern int num;

double f(double t, double y, double z) {
	return z;
}


double mysin(int  y) {
	double a, b;

	a = course[y / discrete].grad;
	b = course[y / discrete].dis;
	return  a / discrete;
}

#if RESFALSE
double g(double t, double y, double z, double Watt) {
	if(z == 0)
		return sqrt(Watt * 2 / M1);
	return Watt / (z*M1);
}

#else

double g(double t, double y, double z, double Watt) {
	if(z == 0)
		return sqrt(Watt * 2 / M1);
	return Watt / (z*M1) - (D / M1)* z*z - 9.8*mysin((int)y);
}

#endif

int cal_time(std::vector<int> W) {
	double k1, k2, k3, k4, l1, l2, l3, l4;
	double y = 0, h = 1, t = 0, v = 0;
	int i;

	while(y < mdistance) {
		i = y / 200;
		k1 = f(t, y, v)*h;
		l1 = g(t, y, v, (double)W[i])*h;
		k2 = f(t + h / 2, y + k1 / 2, v + l1 / 2)*h;
		l2 = g(t + h / 2, y + k1 / 2, v + l1 / 2, (double)W[i])*h;
		k3 = f(t + h / 2, y + k2 / 2, v + l2 / 2)*h;
		l3 = g(t + h / 2, y + k2 / 2, v + l2 / 2, (double)W[i])*h;
		k4 = f(t + h, y + k3, v + l3)*h;
		l4 = g(t + h, y + k3, v + l3, (double)W[i])*h;
		y += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		v += (l1 + 2 * l2 + 2 * l3 + l4) / 6;
		if(v <= 0.0)
			return INT_MAX;

		t += h;
	}

	return (int)t;
}

void writeData(Gene *gene) {
	double k1, k2, k3, k4, l1, l2, l3, l4;
	double y = 0, h = 1, t = 0, z = 0;
	int i;

	CFile fp;
	CFileException ErrorH;
	CString data, filename;
	
	CFileDialog dlg(FALSE, _T("dat"), _T("data"), OFN_OVERWRITEPROMPT,
		_T("データファイル (*.dat)\0*.txt\0\0"));

	dlg.DoModal();
	filename = dlg.GetPathName();

	data.Empty();
	data.Format(_T("%lf %.12lf %.12lf %d %2.1lf\n"), 0.0, 0.0, 0.0, 0.0, 0.0);

	while(y < mdistance) {
		i = y / 200;
		k1 = f(t, y, z)*h;
		l1 = g(t, y, z, gene->w[i])*h;
		k2 = f(t + h / 2, y + k1 / 2, z + l1 / 2)*h;
		l2 = g(t + h / 2, y + k1 / 2, z + l1 / 2, gene->w[i])*h;
		k3 = f(t + h / 2, y + k2 / 2, z + l2 / 2)*h;
		l3 = g(t + h / 2, y + k2 / 2, z + l2 / 2, gene->w[i])*h;
		k4 = f(t + h, y + k3, z + l3)*h;
		l4 = g(t + h, y + k3, z + l3, gene->w[i])*h;
		y += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		z += (l1 + 2 * l2 + 2 * l3 + l4) / 6;
		t += h;
		data.AppendFormat(_T("%lf %.12lf %.12lf %d %2.1lf\n"), t, z, y, gene->w[i], course[i].grad);
		
	}

	if(!fp.Open(filename, CFile::modeCreate | CFile::modeWrite, &ErrorH)) {
		AfxMessageBox(_T("Could not open the file"));
		exit(EXIT_FAILURE);
	}

	fp.Write(data.GetString(), data.GetLength());
}