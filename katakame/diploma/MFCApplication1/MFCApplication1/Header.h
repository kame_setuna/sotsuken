#ifndef H
#define H



#define YES 1
#define NO 0

#define M1 60.0
#define CdA 0.4
#define p 1.220
#define D (CdA * p *0.5)
#define MAX 300

#define GENE_NUM 500
#define GENERATION 5000
#define ELETE 5
#define MUTATION_RATIO 0.1 
#define CROSSOVER_RATIO 0.3

typedef struct {
	double dis;
	double grad;
} C;

class Gene {
public:
	std::vector<int> w;
	int time;

};

int cal_time(std::vector<int> W);
void initialize(Gene gene_group[GENE_NUM]);
void calculate_fitness(Gene gene[GENE_NUM]);
void selection(Gene gene_group[GENE_NUM]);
void mutation(Gene gene_group[GENE_NUM]);
int find_best(Gene gene[GENE_NUM], int pivot);
void writeData(Gene *gene);
void printProcess(Gene gene_group[GENE_NUM]);
void makeChildren(Gene gene_group[GENE_NUM]);

#endif
