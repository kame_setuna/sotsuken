var map;
var directionsDisplay;
var mk;
var mk_array = [];
var waypts = [];
var elev_result = [];
var final_result = [];

function initMap() {
	elev = new google.maps.ElevationService();
	directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer();
	elevationService = new google.maps.ElevationService();

	var opts = {
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(36.374622, 139.022347)
	};
	map = new google.maps.Map(document.getElementById("map"), opts);


	directionsDisplay.setMap(map);

	google.maps.event.addListener(map, 'click', clickEventFunc);

	
}
	

function clickEventFunc(event) {
	var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

	mk = new google.maps.Marker({
		map: map,
		position: latlng
	});

	mk.setMap(map);
	mk_array.push(mk);

}

function removeMarker() {
	for (var i = 0; i < mk_array.length; i++) {
		mk_array[i].setMap(null);
	}

	mk_array = [];
}

function attachMessage(marker, msg) {
	google.maps.event.addListener(marker, "click", function (event) {
		new google.maps.InfoWindow({
			content: msg
		}).open(marker.getMap(), marker);
	});
}

function routing() {
	for (var i = 1; i < mk_array.length - 1; i++) {
		waypts.push({
			location: mk_array[i].getPosition()
		});
	}

	var req = {
		origin: mk_array[0].getPosition(),
		destination: mk_array[mk_array.length - 1].getPosition(),
		waypoints: waypts,
		travelMode: google.maps.TravelMode.DRIVING
	};

	directionsService.route(req, dirResultCallback);
}

function setSamples(legs){
	var journey = 0;
	for(var i in legs){
		journey += legs[i].distance.value;
	}

	return Math.round(journey /200);
}

function dirResultCallback(result, status) {
	if (status == google.maps.DirectionsStatus.OK) {
		directionsDisplay.setDirections(result);

		elevationService.getElevationAlongPath({
			path: result.routes[0].overview_path,
			samples: setSamples(result.routes[0].legs)
		}, elevResultCallback);

	} else {
		alert(status);
	}
}

function elevResultCallback(results, status) {
	if (status == google.maps.ElevationStatus.OK) {
		for (var i in results) {
			var elevation = results[i].elevation;

			var marker = new google.maps.Marker({
				position: results[i].location,
				map: map
			});
			console.log(elevation);
			elev_result.push( {
				elev: elevation,
				latlng:results[i].location
			});
			attachMessage(marker, "標高 " + elevation + "m!!");
		}


		saveresults_p();
	} else if (status == google.maps.ElevationStatus.INVALID_REQUEST) {
		alert("ElevationRequestに問題アリ！渡している内容を確認せよ！！");
	} else if (status == google.maps.ElevationStatus.OVER_QUERY_LIMIT) {
		alert("短時間にElevationRequestクエリを送りすぎ！落ち着いて！！");
	} else if (status == google.maps.ElevationStatus.REQUEST_DENIED) {
		alert("このページでは ElevationRequest の利用が許可されていない！・・・なぜ！？");
	} else if (status == google.maps.ElevationStatus.UNKNOWN_ERROR) {
		alert("ElevationServiceで原因不明のなんらかのトラブルが発生した模様。");
	} else {
		alert("えぇ?っと・・、ElevationService バージョンアップ？");
	}
}

//function distcalc(res, sta) {
//	if (sta == google.maps.DirectionsStatus.OK) {
//		final_result.push({
//			dis: res.routes[0].legs[0].distance.value,
//			elev: elev_result[final_result.length].elev
//		});
//	} else if(sta == google.maps.DirectionsStatus.NOT_FOUND) {
//		alert("NOT_FOUND");
//	} else if (sta == google.maps.DirectionsStatus.ZERO_RESULTS) {
//		alert("ZERO_RESULTS");
//	} else if (sta == google.maps.DirectionsStatus.MAX_WAYPOINTS_EXCEEDED) {
//		alert("MAX_WAYPOINTS_EXCEEDED");
//	} else if (sta == google.maps.DirectionsStatus.INVALID_REQUEST) {
//		alert("INVALID_REQUEST");
//	} else if (sta == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
//		alert("OVER_QUERY_LIMIT");
//	} else if (sta == google.maps.DirectionsStatus.REQUEST_DENIED) {
//		alert("REQUEST_DENIED");
//	} else if (sta == google.maps.DirectionsStatus.UNKNOWN_ERROR) {
//		alert("UNKNOWN_ERROR");
//	}
//}

function saveresults_p() {
	console.log(elev_result.length);
	for (var i = 1; i < elev_result.length; i++) {
		var distance;
		var slope;

		distance = google.maps.geometry.spherical.computeDistanceBetween(elev_result[i - 1].latlng, elev_result[i].latlng);
		slope = elev_result[i].elev - elev_result[i - 1].elev;
		final_result.push({
			distance: distance,
			slope: slope
		});
	}
	//	var req = {
	//		origin: elev_result[i-1].latlng,
	//		destination: elev_result[i].latlng,
	//		travelMode: google.maps.TravelMode.DRIVING,
	//	};

	//	directionsService.route(req, distcalc);
	//	Sleep(1000);
	//}
}

function save() {
	var content;
	content = String(final_result[0].distance)
			+ '\t' + String(final_result[0].slope) + '\n';
	for (var i = 1; i < final_result.length; i++) {
		content += String(final_result[i].distance)
			+ '\t' + String(final_result[i].slope) + '\n';
	}

	var blob = new Blob([content], { "type": "text/plain" });

	var a = document.createElement('a');
	a.download = "courseprofile.txt";
	a.target = '_blank';

	if (window.navigator.msSaveBlob) {
		window.navigator.msSaveBlob(blob, "courseprofile.txt");
	} else if(window.webkitURL && window.webkitURL.createObject){
		a.href = window.webkitURL.createObjectURL(blob);
		a.click();
	}	
}

function Sleep(T) {
	var d1 = new Date().getTime();
	var d2 = new Date().getTime();
	while (d2 < d1 +  T) {    //T秒待つ 
		d2 = new Date().getTime();
	}
	return;
}