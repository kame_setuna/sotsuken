#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double t, double y, double z) {
	return z;
}

double g(double t, double y, double z) {
	return -9.8;
}

int main(void) {
	int i = 0;
	double h, y, z, end, t = 0;
	double k1, k2, k3, k4, l1, l2, l3, l4;
	FILE *fp_data;


	if((fp_data = fopen("data.dat", "w")) == NULL) {
		fputs("Could not open the file", stderr);
		exit(EXIT_FAILURE);
	}

	fputs("h z0 y0\n", stdout);
	scanf("%lf %lf %lf", &h, &z, &y);

	fprintf(fp_data, "0 %.12lf %.12lf\n", z, y);

	while(y >= 0) {
		k1 = f(t, y, z)*h;
		l1 = g(t, y, z)*h;
		k2 = f(t + h / 2, y + k1 / 2, z + l1 / 2)*h;
		l2 = g(t + h / 2, y + k1 / 2, z + l1 / 2)*h;
		k3 = f(t + h / 2, y + k2 / 2, z + l2 / 2)*h;
		l3 = g(t + h / 2, y + k2 / 2, z + l2 / 2)*h;
		k4 = f(t + h, y + k3, z + l3)*h;
		l4 = g(t + h, y + k3, z + l3)*h;
		y += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		z += (l1 + 2 * l2 + 2 * l3 + l4) / 6;
		t += h;
		fprintf(fp_data, "%lf %.12lf %.12lf\n", t, z, y);
	}

	fclose(fp_data);

	return 0;
}