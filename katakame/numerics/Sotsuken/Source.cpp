#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define M1 60.0
#define Watt 210.0
#define CdA 0.4
#define p 1.220
#define D (CdA * p *0.5)

typedef struct {
	double dis;
	double grad;
}C;

C *course;
double dis, dsc;

double mysin(int  y) {
	double a, b;
	a = course[y / (int)dsc].grad;
	b = sqrt(dsc*dsc + course[y / (int)dsc].grad*course[y / (int)dsc].grad);
	return  a / b;
}

double f(double t, double y, double z) {
	return z;
}

double g(double t, double y, double z) {
	if(z == 0)
		return sqrt(Watt * 2 / M1);
	return Watt / (z*M1) - (D / M1)* z*z - 9.8*mysin((int)y);
}

int main(void) {
	int i = 0;
	double h, y, z, end, t = 0;
	double k1, k2, k3, k4, l1, l2, l3, l4;
	FILE* fp_data, *fp_course;

	if((fp_data = fopen("data.dat", "w")) == NULL) {
		fputs("Could not open the file", stderr);
		exit(EXIT_FAILURE);
	}

	if((fp_course = fopen("courseprofile.txt", "r")) == NULL) {
		fputs("Could not open the file.", stderr);
		exit(EXIT_FAILURE);
	}

	fputs("h z0 y0\n", stdout);
	scanf("%lf %lf %lf", &h, &z, &y);

	fprintf(fp_data, "0 %.12lf %.12lf\n", z, y);

	fscanf(fp_course, "%lf %lf", &dis, &dsc);
	course = (C *)malloc(sizeof(C)*(dis / dsc + 1));
	while(EOF != fscanf(fp_course, "%lf %lf", &course[i].dis, &course[i].grad))
		i++;

	while(y < dis) {
		k1 = f(t, y, z)*h;
		l1 = g(t, y, z)*h;
		k2 = f(t + h / 2, y + k1 / 2, z + l1 / 2)*h;
		l2 = g(t + h / 2, y + k1 / 2, z + l1 / 2)*h;
		k3 = f(t + h / 2, y + k2 / 2, z + l2 / 2)*h;
		l3 = g(t + h / 2, y + k2 / 2, z + l2 / 2)*h;
		k4 = f(t + h, y + k3, z + l3)*h;
		l4 = g(t + h, y + k3, z + l3)*h;
		y += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		z += (l1 + 2 * l2 + 2 * l3 + l4) / 6;
		t += h;
		fprintf(fp_data, "%lf %.12lf %.12lf\n", t, z, y);
	}

	fclose(fp_data);
	fclose(fp_course);
	free(course);

	return 0;
}