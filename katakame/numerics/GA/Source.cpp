#define _CRT_SECURE_NO_WARNINGS
#include "stdHeader.h"
#include "Header.h"



#define PrintProcess YES

C course[1000];
double distance;
int discrete, num;

int main(void) {
	int i = 0, j;
	FILE *fp_course;
	Gene gene_group[GENE_NUM];

	if(fopen_s(&fp_course, "courseprofile.txt", "r")) {
		fprintf_s(stderr, "Error!");
		exit(EXIT_FAILURE);
	}

	
	course[0].dis = course[0].grad = 0;
	i = 1;

	while(2 == fscanf_s(fp_course, "%lf %lf", &course[i].dis, &course[i].grad)) {
		course[i].dis = 200;
		i++;
	}

	fclose(fp_course);

	for(j = 2; j < i; j++)
		course[j].dis += course[j - 1].dis;


	distance = course[i - 1].dis;
	discrete = 200;

	num = i;

	srand((unsigned int)time(NULL));

	initialize(gene_group);
	calculate_fitness(gene_group);

	for(i = 0; i < GENERATION; i++) {
#if PrintProcess
		printf("GENERATION:%d\n", i + 1);
		printProcess(gene_group);
#endif
		selection(gene_group);
		makeChildren(gene_group);
		mutation(gene_group);
		calculate_fitness(gene_group);
	}

	i = find_best(gene_group, 0);
	writeData(&gene_group[i]);
	


	return 0;	
}