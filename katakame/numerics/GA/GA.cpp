#define _CRT_SECURE_NO_WARNINGS
#include "stdHeader.h"
#include "Header.h"

extern int num;

void initialize(Gene gene_group[GENE_NUM]) {
	int i, j;
	int sum, r;

	for(i = 0; i < GENE_NUM; i++) {
		sum = 0;
		for(j = 0; j < num; j++) {
			gene_group[i].w.push_back(rand() % (MAX + 1));
			sum += gene_group[i].w.at(j) - 200;
		}

		while(abs(sum) >= num) {
			r = sum / num;
			sum = 0;
			for(j = 0; j < num; j++) {
				gene_group[i].w.at(j) -= r;
				if(gene_group[i].w.at(j) > MAX) {
					sum += MAX - gene_group[i].w.at(j);
					gene_group[i].w.at(j) = MAX;
				} else if(gene_group[i].w.at(j) < 0) {
					sum += -gene_group[i].w.at(j);
					gene_group[i].w.at(j) = 0;
				}
			}
		}


		/*for(j = 0; j < num; j++)
		sum += a[j];

		count = 0;
		for(j = 0; j < num; j++) {
		if(((double)gene_group[i].w[j] - (double)sum / (num - j - count)) >= 80
		&& ((double)gene_group[i].w[j] - (double)sum / (num - j - count)) <= MAX) {
		gene_group[i].w[j] += (double)sum / (num - j - count);
		sum -= (double)sum / (num - j - count);
		} else
		count++;
		}
		gene_group[i].fitness = 200 * num + sum;*/
	}
}

void calculate_fitness(Gene gene[GENE_NUM]) {
	int i, sum = 0;
	for(i = 0; i < GENE_NUM; i++) {
		gene[i].time = cal_time(gene[i].w);
	}
}

int find_best(Gene gene[GENE_NUM], int pivot) {
	int time = INT_MAX;
	int index;
	for(int i = pivot; i < GENE_NUM; i++) {
		if(time > gene[i].time) {
			time = gene[i].time;
			index = i;
		}
	}

	return index;
}

void selection(Gene gene_group[GENE_NUM]) {
	int i, j, index;
	Gene tmp;

	for(i = 0; i < ELETE; i++) {
		index = find_best(gene_group, i);

		tmp.time = gene_group[i].time;
		for(j = 0; j < num; j++)
			tmp.w.push_back(gene_group[i].w[j]);

		gene_group[i].time = gene_group[index].time;
		for(j = 0; j < num; j++)
			gene_group[i].w[j] = gene_group[index].w[j];

		gene_group[index].time = tmp.time;
		for(j = 0; j < num; j++)
			gene_group[index].w[j] = tmp.w[j];
	}
}

void mutation(Gene gene_group[GENE_NUM]) {
	double r, value, d;
	int i, j, k;

	for(i = ELETE; i < GENE_NUM; i++) {
		r = (double)rand() / RAND_MAX;
		if(r <= MUTATION_RATIO) {
			for(j = 0; j < num; j++) {
				r = (double)rand() / RAND_MAX;
				if(r <= MUTATION_RATIO) {
					value = rand() % (MAX + 1);
					d = value - gene_group[i].w[j];
					gene_group[i].w[j] = value;

					for(k = 0; k < num; k++) {
						gene_group[i].w[(j + 1 + k) % num] -= d;
						d = 0;
						if(gene_group[i].w[(j + 1 + k) % (num)] > MAX) {
							d = MAX - gene_group[i].w[(j + 1 + k) % (num)];
							gene_group[i].w[(j + 1 + k) % (num)] = MAX;
						} else if(gene_group[i].w[(j + 1 + k) % (num)] < 0) {
							d = -gene_group[i].w[(j + 1 + k) % (num)];
							gene_group[i].w[(j + 1 + k) % (num)] = 0;
						}

						if(d == 0)
							break;
					}
				}
			}
		}
	}
}

void printProcess(Gene gene_group[GENE_NUM]) {
	printf("%d  ", gene_group[find_best(gene_group, 0)].time);
	puts("\n");
}

void makeChildren(Gene gene_group[GENE_NUM]) {
	int i, j, index, sum, r;
	Gene children, p1, p2, tmp[5];



	for(i = ELETE; i < GENE_NUM; i++) {
		for(j = 0; j < 5; j++) {
			tmp[j] = gene_group[rand() % (GENE_NUM)];
		}
		for(j = 1; j < 5; j++) {
			if(tmp[0].time > tmp[j].time)
				tmp[0] = tmp[j];
		}
		p1 = tmp[0];
		for(j = 0; j < 5; j++) {
			tmp[j] = gene_group[rand() % (GENE_NUM)];
		}
		for(j = 1; j < 5; j++) {
			if(tmp[0].time > tmp[j].time)
				tmp[0] = tmp[j];
		}
		p2 = tmp[0];

		index = rand() % (num);

		sum = 0;
		for(j = 0; j < num; j++) {
			if(j < index)
				children.w.push_back(p1.w[j]);
			else
				children.w.push_back(p2.w[j]);
			sum += (double)children.w[j] - 200;
		}

		while(abs(sum) >= (num)) {
			r = sum / (num);
			sum = 0;
			for(j = 0; j < num; j++) {
				children.w[j] -= r;
				if(children.w[j] > MAX) {
					sum += MAX - children.w[j];
					children.w[j] = MAX;
				} else if(children.w[j] < 0) {
					sum += -children.w[j];
					children.w[j] = 0;
				}
			}
		}

		for(j = 0; j < num; j++)
			gene_group[i].w[j] = children.w[j];
	}
}