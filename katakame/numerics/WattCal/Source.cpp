#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int i = 0, j, sum = 0;
	double a, b, c;
	FILE *fp_course;

	if((fp_course = fopen("data.dat", "r")) == NULL) {
		fputs("Could not open the file.", stderr);
		exit(EXIT_FAILURE);
	}

	while(EOF != fscanf(fp_course, "%lf	%lf	%lf	%d\n", &a, &b, &c ,&j)) {
		sum += j;
		i++;
	}

	printf("%d\n", sum / i);
	return 0;
}